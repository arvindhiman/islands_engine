defmodule IslandsEngine.IslandTest do

  alias IslandsEngine.{Island, Coordinate}

  use ExUnit.Case, async: false

  setup do
    {:ok, island} = IslandsEngine.Island.start_link
    {:ok, island: island}
  end

  test "newly created island is not forested", %{island: island} do

    {:ok, coordinate1} = Coordinate.start_link
    Island.replace_coordinate(island, [coordinate1])

    assert Island.forested?(island) == false

  end

  test "when all coordinates in island are hit then it is forested", %{island: island} do

    coordinates = add_mock_coordinates_to_island(island)

    # guess each coordiante
    Enum.each(coordinates, fn coord -> Coordinate.guess(coord) end)

    assert Island.forested?(island) == true

  end

  defp add_mock_coordinates_to_island(island) do
    # mock coordinates and set them in island
        {:ok, coordinate1} = Coordinate.start_link
        {:ok, coordinate2} = Coordinate.start_link
        coordinates = [coordinate1, coordinate2]

        Coordinate.set_all_in_island(coordinates, :island1)

        Island.replace_coordinate(island, coordinates)
        coordinates
  end

end