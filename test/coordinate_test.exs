defmodule IslandsEngine.CoordinateTest do

  alias IslandsEngine.Coordinate

  use ExUnit.Case, async: false

  setup do
    {:ok, coordinate} = IslandsEngine.Coordinate.start_link
    {:ok, coordinate: coordinate}
  end

  test "newly created coordinate is not guessed", %{coordinate: coordinate} do

    assert Coordinate.guessed?(coordinate) == false

  end

  test "newly created coordinate is not in island", %{coordinate: coordinate} do

      assert Coordinate.in_island?(coordinate) == false

  end

  test "when coordinate is set in island then it is in the island", %{coordinate: coordinate} do

    Coordinate.set_in_island(coordinate, :island1)
    assert Coordinate.in_island?(coordinate) == true

  end

  test "when coordinate is not in island it returns :none", %{coordinate: coordinate} do

    assert Coordinate.island(coordinate) == :none

  end


  test "a coordinate is hit when it is in island and it has been guessed", %{coordinate: coordinate} do

    Coordinate.set_in_island(coordinate, :island1)
    Coordinate.guess(coordinate)
    assert Coordinate.hit?(coordinate) == true

  end

  test "when multiple coordinates are set in island then each coordiante is in the isalnd" do

    {:ok, coordinate1} = Coordinate.start_link
    {:ok, coordinate2} = Coordinate.start_link

    Coordinate.set_all_in_island([coordinate1, coordinate2], :island1)

    assert Coordinate.in_island?(coordinate1) == true
    assert Coordinate.in_island?(coordinate2) == true

  end

end
