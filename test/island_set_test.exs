defmodule IslandsEngine.IslandSetTest do

  alias IslandsEngine.{Island, Coordinate, IslandSet}

  use ExUnit.Case, async: false

  setup do
    {:ok, island_set} = IslandsEngine.IslandSet.start_link
    {:ok, island_set: island_set}
  end

  test "newly created island_set is initialized", %{island_set: island_set} do

    IO.puts "#{IslandSet.to_string(island_set)}"

    island = Agent.get(island_set, fn state -> Map.get(state, :atoll) end)

    assert island != :none

  end

  test "setting coordinates using island key in an island_set sets coordinates in the island", %{island_set: island_set} do

    coordinates = add_mock_coordinates_to_island(island_set, :atoll)
    [coordinate1|_list] = coordinates

    assert Coordinate.in_island?(coordinate1) == true

    island_key = Coordinate.island(coordinate1)
    assert island_key == :atoll

  end

  test "a new island set with coordinates is not forested", %{island_set: island_set} do

   Enum.each(IslandSet.keys, fn key -> add_mock_coordinates_to_island(island_set, key) end)

   assert IslandSet.all_forested?(island_set) == false

  end

  defp add_mock_coordinates_to_island(island_set, island_key) do
    # mock coordinates and set them in island
    {:ok, coordinate1} = Coordinate.start_link
    {:ok, coordinate2} = Coordinate.start_link
    coordinates = [coordinate1, coordinate2]

    IslandSet.set_island_coordinates(island_set, island_key, coordinates)
    coordinates
  end

end