defmodule IslandsEngine.BoardTest do

  alias IslandsEngine.{Island, Coordinate, Board}

  use ExUnit.Case, async: false

  setup do
    {:ok, board} = IslandsEngine.Board.start_link
    {:ok, board: board}
  end

  test "newly created board is initialized with coordinates", %{board: board} do

    #IO.puts Board.to_string(board)
    coordinate_a1 = Board.get_coordinate(board, :a1)
    assert Coordinate.to_string(coordinate_a1) != nil

    coordinate_j10 = Board.get_coordinate(board, :j10)
    assert Coordinate.to_string(coordinate_j10) != nil

  end

  test "guessing a coordinate sets it to guessed", %{board: board} do

      coordinate_a1 = Board.get_coordinate(board, :a1)

      assert Coordinate.guessed?(coordinate_a1) == false

      Board.guess_coordinate(board, :a1)
      assert Coordinate.guessed?(coordinate_a1) == true

  end

  test "setting a coordinate in island sets it in island", %{board: board} do

    Board.set_coordinate_in_island(board, :a1, :my_island)

    coordinate_a1 = Board.get_coordinate(board, :a1)

    assert Coordinate.in_island?(coordinate_a1) == true

  end

  test "when a coordinate is guessed and is in island then it is hit", %{board: board} do
    Board.set_coordinate_in_island(board, :a1, :my_island)
    Board.guess_coordinate(board, :a1)

    assert Board.coordinate_hit?(board, :a1) == true

  end


end