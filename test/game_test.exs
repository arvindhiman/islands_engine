defmodule IslandsEngine.GameTest do

  use ExUnit.Case, async: false

  alias IslandsEngine.{Game, Board, Coordinate, Player}

  setup do
    {:ok, game} = Game.start_link("Arvind")
    {:ok, game: game}
  end

  test "Game is initialized with name", %{game: game} do

    state = Game.call_demo(game)
    assert Player.get_name(state.player1) == "Arvind"

  end

  test "Adding a name sets player2 name", %{game: game} do
    Game.add_player(game, "Seema")

    state = Game.call_demo(game)
    assert Player.get_name(state.player2) == "Seema"
    assert Player.get_name(state.player1) == "Arvind"

  end

  test "setting coordinates in players island sets coordinates in island", %{game: game} do

    Game.set_island_coordinates(game, :player1, :dot, [:a1])

    state = Game.call_demo(game)

    player = Map.get(state, :player1)

    board = Player.get_board(player)

    coordinate = Board.get_coordinate(board, :a1)

    assert Coordinate.in_island?(coordinate) == true

  end

end